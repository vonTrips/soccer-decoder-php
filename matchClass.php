<?php

/**
 * matchClass je jednoducha trida, implementujici metodu soccer-decoder v jazyku PHP
 * soccer-decoder slouzi k simulaci fotbalovych utkani
 * http://www.iaees.org/publications/journals/Selforganizology/articles/2014-1(3-4)/a-new-game-theory-algorithm-simulates-soccer-matches.pdf
 *
 * @author Dan
 */
class matchClass {

    protected $attackTeam;
    protected $defendTeam;
    protected $currentFieldzone;
    protected $homeScore;
    protected $awayScore;
    protected $playActions = 1;
    protected $currentPlayAction = 0;
    protected $debug;
    protected $debugPrint;
    protected $counterAttack;
    protected $teams = array();
    protected $teamData = array("home" => array(), "away" => array());
    protected $skills = array('gkSkill', 'defSkill', 'midSkill', 'offSkill');
    protected $decrease = array('gkDecrease', 'defDecrease', 'midDecrease', 'offDecrease');
    protected $zones = array('SHOOT H', 'GOAL H', 'FIELD H', 'MIDFIELD', 'FIELD A', 'GOAL A', 'SHOOT A');

    /**
     * Je mozne, ze v ramci konstruktoru dojde k predani objektu DB
     */
    function __construct() {
        
    }

    /**
     * Hlavni funkce simulujici jeden zapas.
     * Ocekava 3 povinne vstupy (2x pole s tymy a jednou pocet akci na zapas)
     * @param array $homeTeam domaci tym
     * @param array $awayTeam hostujici tym
     * @param int $actions pocet akci na zapas
     * @param bool $result tisknout vysledek? (vychozi ANO=TRUE)
     * @param bool $return vracet zapasovy status? (vychozi NE=FALSE)
     * @param bool $debug vypis dat jednoho utkani (vychozi NE=FALSE)
     * @return string vraci H (vyhra domacich), A (vyhra hosti), D (remiza)
     */
    function simulateMatch(array $homeTeam, array $awayTeam, int $actions, bool $result = true, bool $return = false, bool $debug = false) {
        $this->teamData['home'] = $homeTeam;
        $this->teamData['away'] = $awayTeam;
        $this->playActions = $actions;
        $this->debug = $debug;
        $this->initMatch();
        for ($this->currentPlayAction = 0; $this->currentPlayAction < $actions; $this->currentPlayAction++) {
            $this->calculateRatings();
            if ($this->debug) {
                $this->debugPrint = $this->currentPlayAction . ": " . $this->zones[$this->currentFieldzone] . ' - ' . $this->attackTeam . ' vs. ' . $this->defendTeam;
            }
            $battleWinner = $this->matchBattle();                   // souboj o pozici/mic na hristi
            if ($this->debug)
                $this->debugPrint .= '<b>' . $battleWinner . '</b><br>';
            if ($battleWinner == $this->attackTeam) {               // vyhral utok
                if ($battleWinner == "home") {                      // utoci domaci (A)
                    if ($this->currentFieldzone == 6) {             // gol
                        $this->homeScore++;
                        $this->currentFieldzone = 3;
                        $this->changeBallPossesion();
                    } else {                                        //jenom posun na hristi domaci (A)
                        $this->currentFieldzone++;
                    }
                } else {                                            // utoci hoste (B)
                    if ($this->currentFieldzone == 0) {             // gol
                        $this->awayScore++;
                        $this->currentFieldzone = 3;
                        $this->changeBallPossesion();
                    } else {                                        //jenom posun na hristi hoste (B)
                        $this->currentFieldzone--;
                    }
                }
            } else {                                                // vyhrala obrana, otoceni hry a zacatek v midfield (3)
                if ($this->currentFieldzone == 0 || $this->currentFieldzone == 6 || $this->currentFieldzone == 3) {
                    $this->currentFieldzone = 3;
                } else if ($battleWinner == "home") {
                    $this->currentFieldzone++;
                } else {
                    $this->currentFieldzone--;
                }
                $this->changeBallPossesion();
            }
            // tady dochazi k uprave ratingu a casem treba i dalsim vecem
            $this->decreaseSkills();
            echo $this->debugPrint;
        }
        $state = "D";
        if ($this->homeScore > $this->awayScore) {
            $state = "H";
        } else if ($this->homeScore < $this->awayScore) {
            $state = "A";
        }
        if ($result) {
            echo $this->teams['home']['name'] . ' - ' . $this->teams['away']['name'] . ': <b>' . $this->homeScore . ' - ' . $this->awayScore . '</b><br>';
        }
        if ($return) {
            return $state;
        }
    }

    /**
     * Simulace jedne bitvy = souboje o pozici/mic
     * @return string vraci vitezny tym (home/away)
     */
    function matchBattle() {
        $attackRating = 0.0;
        $defendRating = 0.0;
        switch ($this->currentFieldzone) {
            case 0:
            case 6: {
                    $shooter = $this->teamData[$this->attackTeam]['players'][$this->getShootingPlayer()];
                    $attackRating = ($shooter['rating'] * (1 - (((0.3 - $shooter['physDecay']) / $this->playActions) * $this->currentPlayAction)));
                    $defendRating = $this->teams[$this->defendTeam]['gkRating'];
                    break;
                }
            case 2:
            case 4: {
                    $attackRating = $this->teams[$this->attackTeam]['offRating'];
                    $defendRating = $this->teams[$this->defendTeam]['defRating'];
                    break;
                }
            case 1:
            case 5: {
                    $attackRating = $this->teams[$this->attackTeam]['offRating'];
                    $defendRating = $this->teams[$this->defendTeam]['gdRating'];
                    break;
                }
            case 3: {
                    $attackRating = $this->teams[$this->attackTeam]['midRating'];
                    $defendRating = $this->teams[$this->defendTeam]['midRating'];
                    break;
                }
        }
        $beta = 0.5;
        $a = $this->randomErlang($attackRating, $beta);
        $d = $this->randomErlang($defendRating, $beta);

        if ($this->debug)
            $this->debugPrint .= ' ' . round($attackRating, 2) . ' (' . round($a, 2) . ') - ' . round($defendRating, 2) . ' (' . round($d, 2) . ') ';

        if ($a > $d) {     // ten kdo vyhraje určuje další směr hry
            return $this->attackTeam;
        } else {
            return $this->defendTeam;
        }
    }

    /**
     * Urci strilejiciho hrace v zavislosti na pravdepodobnosti (utocnik zakoncuje nejvice, obrance nejmene)
     * Z konkretniho postu urci zakoncujiciho hrace dle jeho vahy (ratignu)
     * Vyuziva k tomu funcki getRandomWeightedElement (viz github)
     * @return string Index strilejiciho hrace
     */
    function getShootingPlayer() {
        if ($this->counterAttack) {     // pri protiutoku 1% pravdepodobnost zakonceni obrancem, 29% zaloznikem, zbytek utocnikem
            $def = 10;
            $mid = 300;
        } else {                        // pri postupnem utoku nebo stanadardce je zakonceni v pomeru obrana 10 %, zaloha 30 % a utok 60 %
            $def = 100;
            $mid = 400;
        }
        $pos = mt_rand(0, 999);
        if ($pos < $def) {
            // zakoncuje obrance
            return $this->getRandomWeightedElement($this->teams[$this->attackTeam]['defenders']);
        } else if ($pos < $mid) {
            // zakoncuje zaloznik
            return $this->getRandomWeightedElement($this->teams[$this->attackTeam]['midfielders']);
        } else {
            // zakoncuje utocnik
            return $this->getRandomWeightedElement($this->teams[$this->attackTeam]['strikers']);
        }
    }

    /**
     * Vypocita zapasove ratingy obou tymu
     */
    function calculateRatings() {
        $this->calculateTeamRatings('home');
        $this->calculateTeamRatings('away');
    }

    /**
     * Vypocita a nastavi tymovy rating jednoho tymu (zavislost na stringu)
     * @param string $team tym pro ktery se ma vypocitat rating (home/away)
     */
    function calculateTeamRatings(string $team) {
        $this->teams[$team]['gkRating'] = $this->teams[$team]['gkSkill'];
        $this->teams[$team]['gdRating'] = $this->teams[$team]['gkSkill'] + $this->teams[$team]['defSkill'] * 0.75;
        $this->teams[$team]['defRating'] = $this->teams[$team]['defSkill'] + $this->teams[$team]['midSkill'] * 0.5;
        $this->teams[$team]['midRating'] = $this->teams[$team]['midSkill'] + $this->teams[$team]['defSkill'] * 0.1 + $this->teams[$team]['offSkill'] * 0.5;
        $this->teams[$team]['offRating'] = $this->teams[$team]['offSkill'] + $this->teams[$team]['midSkill'] * 0.5;
    }

    /**
     * Snizeni tymovych skillu v zavislosti na unave.
     * Bool parametry jsou zde do budoucna, protoze muzou nastat situace, 
     * kdy se budou snizovat skilly jen jednomu tymu (zpravidla ten co hodne beha bez mice)
     * @param bool $home ma se snizit domaci tym? (vychozi ANO=TRUE)
     * @param bool $away ma se snizit hostujici tym? (vychozi ANO=TRUE)
     */
    function decreaseSkills(bool $home = true, bool $away = true) {
        if ($home) {
            $this->decreaseTeamSkills('home');
        }
        if ($away) {
            $this->decreaseTeamSkills('away');
        }
    }

    /**
     * Snizeni konkretnich skillu (urcuje chranena promenna skills)
     * v zavislosti na konkretnim tymovem zhorseni (prumer pro danou pozici)
     * >>>Jeste by to slo resit tak, ze se zhorsi kazdy hrac zvlast,
     * >>>ale zde je rozdil ratingu v setinach, ale vypocet je nahosbne narocnejsi
     * @param string $team tym, ktery se ma menit (home/away)
     */
    function decreaseTeamSkills(string $team) {
        for ($i = 0; $i < 4; $i++) {
            $this->teams[$team][$this->skills[$i]] *= (1 - $this->teams[$team][$this->decrease[$i]]);
        }
    }

    /**
     * Zmena drzeni mice, pouze prohodi utocici a branici se tym
     * Priznak pro counter attack zatim nic neresi (protiutoky nejsou implementovany)
     * @param bool $counterAttack Dochazi pri zmene drzeni mice k protiutoku? (vychozi NE=FALSE)
     */
    function changeBallPossesion(bool $counterAttack = false) {
        $helper = $this->defendTeam;
        $this->defendTeam = $this->attackTeam;
        $this->attackTeam = $helper;
        $this->counterAttack = $counterAttack;
    }

    /**
     * Nastaveni ratingu G, D, M, S podle pozic a zkusenosti
     * @param array $team predavam pole se vsemi udaji o tymu a hracich v pozadovanem tvaru
     * @return array vracim pole se vsemi nastavenimi pro dany tym
     */
    function setTeamRating(string $team) {
        $return = $this->defaultMatchTeamArray();
        $return["name"] = $this->teamData[$team]["name"];
        $return["style"] = $this->teamData[$team]["style"];
        $d = $m = $s = $i = 0;
        $defenders = $midfielders = $strikers = array();
        $rating = 0.0;
        foreach ($this->teamData[$team]['players'] as $player) {
            switch ($player['position']) {
                case "G": {
                        $return['gkSkill'] += $player['rating'];
                        $return['gkDecrease'] = 0.20 - $player['physDecay'];
                        break;
                    }
                case "D": {
                        $return['defSkill'] += $player['rating'];
                        $return['defDecrease'] += 0.3 - $player['physDecay'];
                        $defenders["$i"] = (int)($player['rating'] * 10);
                        $d++;
                        break;
                    }
                case "M": {
                        $return['midSkill'] += $player['rating'];
                        $return['midDecrease'] += 0.4 - $player['physDecay'];
                        $midfielders["$i"] = (int)($player['rating'] * 10);
                        $m++;
                        break;
                    }
                case "S": {
                        $return['offSkill'] += $player['rating'];
                        $return['offDecrease'] += 0.35 - $player['physDecay'];
                        $strikers["$i"] = (int)($player['rating'] * 10);
                        $s++;
                        break;
                    }
            }
            $rating += $player['rating'];
            $i++;
        }
        $return['gkDecrease'] /= ($this->playActions);
        $return['defDecrease'] /= ($d * $this->playActions);
        $return['midDecrease'] /= ($m * $this->playActions);
        $return['offDecrease'] /= ($s * $this->playActions);
        $return['defenders'] = $defenders;
        $return['midfielders'] = $midfielders;
        $return['strikers'] = $strikers;
        $return['avgRating'] = $rating / 11;                                         // prumerny rating tymu, k nicemu se nepouziva, jen me to tak zajima
        
        $this->teams[$team] = $return;
    }

    /**
     * Predpripravene pole pro zapasovy zaznam tymu
     * @return array vracim vychozi zapasove pole pro jeden tym
     */
    private function defaultMatchTeamArray() {
        return array(
            "name" => "",
            "style" => "",
            "gkRating" => 0,
            "defRating" => 0,
            "midRating" => 0,
            "offRating" => 0,
            "gkSkill" => 0,
            "defSkill" => 0,
            "midSkill" => 0,
            "offSkill" => 0,
            "avgRating" => 0,
            "gkDecrease" => 0,
            "defDecrease" => 0,
            "midDecrease" => 0,
            "offDecrease" => 0,
            "defenders" => array(),
            "midfielders" => array(),
            "strikers" => array()
        );
    }

    /**
     * Inicializace utkani (resetovani skore, vychozi zona, zacinajici tym)
     */
    private function initMatch() {
        $this->setTeamRating('home');        
        $this->setTeamRating('away');
        $this->homeScore = 0;
        $this->awayScore = 0;
        $this->currentFieldzone = 3;
        $this->attackTeam = "away";
        $this->defendTeam = "home";
    }

    /**
     * Ukradena implementace Erlang distribution (na rozdil od moji funguje)
     * https://github.com/agurodriguez/soccer-decoder
     * @param float $alpha
     * @param float $beta
     * @return float
     */
    private function randomErlang(float $alpha, float $beta) {
        assert($alpha > 0.0);
        assert($beta > 0.0);
        // Use the Marsaglia and Tsang (2000) method to generate Z ~ Gamma(alpha, 1)
        $d = $alpha - 1.0 / 3.0;
        $c = 1.0 / sqrt(9.0 * $d);
        $z = 0.0;
        for (;;) {
            $x = 0.0;
            do {
                $x = stats_rand_gen_normal(0, 1);
                $v = 1.0 + $c * $x;
            } while ($v <= 0);
            $v = pow($v, 3.0);
            $u = stats_rand_gen_funiform(0.0, 1.0);
            if ($u < 1.0 - 0.0331 * pow($x, 4.0)) {
                $z = $d * $v;
                break;
            }
            if (log($u) < 0.5 * pow($x, 2.0) + $d * (1 - $v + log($v))) {
                $z = $d * $v;
                break;
            }
        }
        // Scale to X ~ Gamma(alpha, beta)
        return $z / $beta;
    }

    /**
     * Vypujcena utilita pro vyber prvku dle jeho vahy, viz oroginalni popis:
     * Utility function for getting random values with weighting.
     * Pass in an associative array, such as array('A'=>5, 'B'=>45, 'C'=>50)
     * An array like this means that "A" has a 5% chance of being selected, "B" 45%, and "C" 50%.
     * The return value is the array key, A, B, or C in this case.  Note that the values assigned
     * do not have to be percentages.  The values are simply relative to each other.  If one value
     * weight was 2, and the other weight of 1, the value with the weight of 2 has about a 66%
     * chance of being selected.  Also note that weights should be integers.
     * https://gist.github.com/irazasyed/f41f8688a2b3b8f7b6df
     * 
     * @param array $weightedValues
     */
    function getRandomWeightedElement(array $weightedValues) {
        $rand = mt_rand(1, (int) array_sum($weightedValues));

        foreach ($weightedValues as $key => $value) {
            $rand -= $value;
            if ($rand <= 0) {
                return $key;
            }
        }
    }

}
