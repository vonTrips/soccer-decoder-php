# Soccer-Decoder PHP

## Description

ENG: This project is base implementation of soccer-decoder (Alessandro Ferrarini's algorithm) in PHP.

CZE: Tento projekt je základní implementací Soccer-Decoder (algoritmus od Alessandra Ferrariniho) v PHP.

Simulace probíhá pomocí matchClass, resp. metody *simulate()*. Ta simuluje zápas mezi dvěma týmy (první dva parametry) v požadované délce playActions (třeří parametr). Ideální nastavení pro tuto implementaci je **100** herních bitev (soubojů o míč).

## Input for simulation
### Teams
U každého týmu očekává pole (**array()**) o třech prvcích:
1. **name** (string) - název týmu
2. **style** (string) - styl hry (zatím nedodstatné, ale povinné)
3. **players** (array) - pole (**array()**) s 11 hráči

`array(
    "name" => "Team name",
    "style" => "CA",
    "players" => array()
);`

#### Players
Pole s hodnotami pro 11 hráčů:
1. **name** (string) - jméno hráče
2. **position** (string) - pozice hráče z výběru (G, D, M, S)
3. **rating** (float) - celkový rating hráče (1.0 až 9.9)
4. **physDecay** (float) - úbytek sil, dopočítává se v *matchClass::setTeamRating* (-0.013 až +0.011)

`array(
    "name" => "Player name",
    "position" => "G",
    "rating" => 7.3,
    "physDecay" => 0.002
);`

### Play Actions
Každá rozehra simuluje "herní situaci" v konkrétní části hřiště. Prvotní je výkop (střed hřiště - MIDFIELD) a zde se útočící tým snaží přehrát bránící tým, aby se posunul na útočnou polovinu (tedy cca za středový kruh - FIELD x). Poté probíhá další souboj o posun vpřed, popř. zastavení útoku až posun k šestnáctce (GOAL x) soupeře. Pokud zde útočící tým uspěje, dostává se k zakončení (SHOOT).

V každé části hřiště spolu "bojují" jiní hráči, resp. ratingy poskládané od všech hráčů s různou mírou zapojení:
- **SHOOT A**: zakončovatel B vs brankář A
- **GOAL A**: útok B vs obrana šestnáctky A
- **FIELD A**: záloha B vs obrana A (asi něco na způsob kreativity vs. bránění)
- **MIDFIELD**: záloha A vs záloha B (prostě souboj o míč)
- **FIELD B**: záloha A vs obrana B
- **GOAL B**: útok A vs obrana šestnáctky B
- **SHOOT B**: zakončovatel A vs brankář B

A vliv pozic hráčů na jednotlivé ratingy:
- **Brankář** = 100 % GK
- **Obrana šestnáctky** = 100 % GK, 75 % D
- **Obrana** = 100 % D, 50% M
- **Záloha** = 100 % M, 50 % S, 10 % D
- **Útok** = 100 % S, 50 % M
